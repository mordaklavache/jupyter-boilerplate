((magit-branch nil)
 (magit-cherry-pick nil
                    ("--ff"))
 (magit-commit nil)
 (magit-diff
  ("--no-ext-diff" "--stat")
  (("--" "Spoke/Info.plist")
   "--no-ext-diff" "--stat"))
 (magit-dispatch nil)
 (magit-fetch nil)
 (magit-log
  ("-n256" "--graph" "--decorate"))
 (magit-merge nil)
 (magit-pull nil)
 (magit-push nil
             ("--force-with-lease")
             ("--force"))
 (magit-rebase nil)
 (magit-remote
  ("-f"))
 (magit-reset nil)
 (magit-stash nil)
 (magit:-- "Spoke/Info.plist" ""))
