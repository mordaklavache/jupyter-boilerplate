#!/bin/bash
UMASK=0022
echo "UMASK OLD VALUE = " `umask`
echo "SETTING UMASK TO $UMASK"
umask $UMASK
echo "UMASK NEW VALUE = " `umask`
echo "CURRENT ENV VAR"
env
echo "EXEC COMMAND" $@
$@
