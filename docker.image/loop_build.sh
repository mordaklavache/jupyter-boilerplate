#!/bin/bash
# USE IT WHEN NETWORK MAY FAIL SOMETIMES
while [ 1 ]; do
	/bin/bash build.sh;
	if [ $? -eq 0 ]; then
		echo -e "build complete";
		break;
	fi
	echo -e "build failed";
	sleep 10;
	echo -e "retrying";
done
exit 0
