#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(void) {
    // Original number is 93
    int color_list[] = \
        {20, 21, 26, 27, 32, 33, 38, 39, \
         43, 44, 45, 47, 48, 49, 55, 56, \
         57, 62, 63, 68, 69, 74, 75, 80, \
         81, 91, 92, 93, 104, 105, 109, 110, \
         111, 151, 152, 153, 156, 157, 158, 159, \
         179, 180, 181, 182, 183, 186, 187, 188, \
         189, 191, 192, 193, 194, 195, 221, 222, \
         223, 224, 225, 229, 230, 231};

    srand(time(NULL));   // Initialization, should only be called once.
    int base = color_list[rand() % (sizeof(color_list) / sizeof(int))];

    char s1[16], s2[16], s3[16], s4[16], s5[16], s6[16], s7[16], s8[16];

    // for (base = 0; base < 255; base++) {
    //     printf("number: %i\n", base);
    {
        sprintf(s1, "\x1b[38;5;%im", base);
        sprintf(s2, "\x1b[38;5;%im", base + 35);
        sprintf(s3, "\x1b[38;5;%im", base + 36);
        sprintf(s4, "\x1b[38;5;%im", base + 70);
        sprintf(s5, "\x1b[38;5;%im", base + 71);
        sprintf(s6, "\x1b[38;5;%im", base + 105);
        sprintf(s7, "\x1b[38;5;%im", base + 106);
        sprintf(s8, "\x1b[38;5;%im", base + 110);

        printf(" %8$so\n"
            "o      %6$s______/%7$s~/~/~/__           %5$s/(%2$s(\n"
            "  %6$so  // __            %5$s====__    %2$s/%3$s_((\n"
            "    %6$s//  %7$s@))       %5$s))))      =%2$s=%3$s=/__((\n"
            "    %6$s)%7$s)           %5$s)))))))        %3$s__((\n"
            "    %7$s\\\\     %4$s\\%5$s)     )))     %3$s__===\\ %1$s_((\n"
            "     %7$s\\\\_%4$s_%5$s___________%2$s_%3$s_====      %1$s\\_((\n"
            "                                 \\\((\n\n",
            s1, s2, s3, s4, s5, s6, s7, s8);
    }
    return 0;
}
