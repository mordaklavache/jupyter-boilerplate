#!/bin/bash
# Use tee to store logs of stderr and stdout and see output
# ./build.sh 2>&1 | tee build.log

# Ensures that the build environment exists, as well as the necessary base images
SCRIPT="$(readlink -f "$0")"
DIR="$(dirname "$SCRIPT")"

docker build --tag "bmickael/rust-ultimate:latest" --rm "$DIR" --progress=plain
exit $?
